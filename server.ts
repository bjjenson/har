///<reference path="./typings/tsd.d.ts" />

import express = require('express');
import bodyParser = require('body-parser')
var app = express();
var port = process.env.PORT || 3000;
var fileLimit = '50mb';
import routes = require('./app/routes');

app.use(express.static('www'));
app.use(bodyParser.urlencoded({
    extended:true,
    limit: fileLimit
}));
app.use(bodyParser.json({
    limit: fileLimit
}));

routes.setupRoutes(app);

console.log('Node app listening on localhost port: ' + port);
app.listen(port);
