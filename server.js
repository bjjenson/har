///<reference path="./typings/tsd.d.ts" />
var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3000;
var fileLimit = '50mb';
var routes = require('./app/routes');
app.use(express.static('www'));
app.use(bodyParser.urlencoded({
    extended: true,
    limit: fileLimit
}));
app.use(bodyParser.json({
    limit: fileLimit
}));
routes.setupRoutes(app);
console.log('Node app listening on localhost port: ' + port);
app.listen(port);
//# sourceMappingURL=server.js.map