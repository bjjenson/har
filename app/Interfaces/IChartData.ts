///<reference path="../../typings/tsd.d.ts" />

export = IChartData

interface IChartData {
    key:string;
    values:Array<Array<any>>;
}