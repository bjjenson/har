///<reference path="../../typings/tsd.d.ts" />

export = IMultiStatistic;

interface IMultiStatistic<T> {
    key:string;
    values:Array<T>
}
