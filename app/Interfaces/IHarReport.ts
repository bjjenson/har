///<reference path="../../typings/tsd.d.ts" />

import IMultiStatistic = require('./IMultiStatistic');
import IChartData = require('./IChartData');

export = IHarReport;

interface IHarReport {
    pageTitle:string;
    requestCount:number;
    pageLoadDuration:number;
    requestsByDuration:Array<IChartData>;
    requestsByType:Array<IMultiStatistic<number>>;
    requestsReferred:Array<IMultiStatistic<string>>;
}
