///<reference path="../typings/tsd.d.ts" />

import IMultiStatistic = require('./Interfaces/IMultiStatistic');
import IHarReport = require('./Interfaces/IHarReport');
import IChartData = require('./Interfaces/IChartData');

export = Parser;

class Parser {
    constructor(private data:string) {

    }

    private getLog():any {
        return this.data['log'];
    }

    getEntryCount():number {
        return this.getLog().entries.length;
    }

    getPageLoadDuration():number {
        return this.getLog().pages[0].pageTimings.onLoad;
    }

    getTitle():string {
        return this.getLog().pages[0].title;
    }

    getEntriesByDuration():Array<IChartData> {
        var entries = this.sortByKey(this.getLog().entries, "time", true);
        var report = {
            key: 'Requests by Duration',
            values: entries.map((e):any=> {
                return [
                    e.request.url,
                    e.time
                ];
            })
        };
        return [report];
    }

    private sortByKey(entries:Array<any>, key:string, descending:boolean):Array<any> {
        var order = descending ? 1 : -1;
        return entries.sort((a, b) => {
            var x = a[key];
            var y = b[key];
            return ((x < y) ? order : ((x > y) ? order * -1 : 0));
        });
    }

    getEntriesByType():Array<IMultiStatistic<number>> {
        var groups = this.groupBy(this.getLog().entries);

        return Parser.groupToList<number>(groups);
    }

    groupBy(list:any) {
        var groups = {};
        list.forEach((item)=> {
            var key = item.response.content.mimeType;
            var size = item.response.content.size;
            this.addToGroup(groups, key, size);
        })
        return groups;
    }

    private addToGroup(groups:any, key:string, value:any):any {
        if (key in groups) {
            groups[key].push(value)
        } else {
            groups[key] = [value]
        }
    }

    private static groupToList<T>(groups):Array<IMultiStatistic<T>> {
        var list = Array<IMultiStatistic<T>>();
        for (var i in groups) {
            list.push({
                key: i, values: groups[i]
            });
        }

        return list;
    }

    getRequestsReferred():Array<IMultiStatistic<string>> {
        var groups = {};
        this.getLog().entries.forEach((e)=> {
            var referrer = e.request.headers.filter((f)=> {
               return f.name.toLowerCase() =="referer";
            });
            if(referrer) {
                var key = referrer[0].value;
                var value = e.request.url;
                this.addToGroup(groups, key, value);
            }
        });

        return Parser.groupToList<string>(groups);
    }

    createReport():IHarReport {
        return <IHarReport>{
            pageTitle: this.getTitle(),
            requestCount: this.getEntryCount(),
            pageLoadDuration: this.getPageLoadDuration(),
            requestsByDuration: this.getEntriesByDuration(),
            requestsByType: this.getEntriesByType(),
            requestsReferred: this.getRequestsReferred()
        };
    }
}