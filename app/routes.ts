///<reference path="../typings/tsd.d.ts" />

import express = require('express')
import harParser = require('./harParser')

export function setupRoutes(app:express.Application) {
    app.post('/har', (req:express.Request, res:express.Response)=> {
        var parser = new harParser(req.body);
        var report = parser.createReport();
        //console.log(req.body);
        res.setHeader('Content-Type', 'application/json');
        res.send(JSON.stringify(report));
    });
}