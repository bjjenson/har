module.exports = function (grunt) {

    grunt.initConfig({
        typescript: {
            base: {
                src: [
                    'app/**/*.ts'
                ],
                options: {
                    module: 'commonjs',
                    target: 'es5',
                    sourceMap: true
                }
            }
        },
        concat: {
            options: {
                separator: '\n'
            },
            dist: {
                // the files to concatenate
                src: ['www/app/app.js', 'www/app/**/*.js'],
                // the location of the resulting JS file
                dest: 'www/build/app.js'
            }
        }
    });

    grunt.registerTask('bower', 'Installing bower dependencies', function () {
        var exec = require('child_process').exec;
        var cb = this.async();
        exec('bower install', {}, function (err, stdout, stderr) {
            console.log(stdout);
            cb();
        });
    });

    grunt.registerTask('install-tsd', 'Installing typescript definitions', function () {
        var exec = require('child_process').exec;
        var cb = this.async();
        exec('tsd install', {}, function (err, stdout, stderr) {
            console.log(stdout);
            cb();
        });
    });

    grunt.registerTask('install-tsd', 'Installing typescript definitions', function (dir) {
        var exec = require('child_process').exec;
        var cb = this.async();
        exec('tsd install', {cwd: dir}, function (err, stdout, stderr) {
            console.log(stdout);
            cb();
        });
    });

    grunt.registerTask('tsd', function() {
        grunt.task.run(['install-tsd']);
        grunt.task.run(['install-tsd:www']);
    });

    grunt.loadNpmTasks('grunt-typescript');
    grunt.loadNpmTasks('grunt-contrib-concat');

    grunt.registerTask('default', ['bower', 'tsd', 'typescript', 'concat']);
}
