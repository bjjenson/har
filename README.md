# README #

HAR Report generator

### What is this repository for? ###

This is Bryan Jenson's HAR job application project for ObservePoint.

### How do I get set up? ###

Pull the Repo then on the command line at the project root run the following.

* npm install
* grunt
* npm start
* browse to localhost:3000

I added some basic error reporting.

* If you try to load a file that is not .har
* If there is a server error when processing the file. 