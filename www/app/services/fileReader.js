///<reference path="../../typings/tsd.d.ts"/>
angular.module("har").service("fileReader", [
    '$q',
    'alertService',
    function ($q, alertService) {
        var onLoad = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };
        var onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };
        var onProgress = function (reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress", {
                    total: event.total,
                    loaded: event.loaded
                });
            };
        };
        var getReader = function (deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };
        var readDataFromFile = function (file, scope) {
            if (!checkFileName(file.name)) {
                return;
            }
            var deferred = $q.defer();
            var reader = getReader(deferred, scope);
            reader.readAsText(file);
            return deferred.promise;
        };
        var checkFileName = function (filename, scope) {
            var extension = filename.split('.').pop();
            if (extension == 'har') {
                return true;
            }
            alertService.addAlert('File must be of type *.har.  Please check the file and try again.');
            return false;
        };
        return readDataFromFile;
    }]);
//# sourceMappingURL=fileReader.js.map