///<reference path="../../typings/tsd.d.ts"/>

angular.module("har").service("fileReader", [
    '$q',
    'alertService',
    function ($q:ng.IQService, alertService) {
    var onLoad = function (reader, deferred:ng.IDeferred<any>, scope:ng.IScope):any {
        return () => {
            scope.$apply(() => {
                deferred.resolve(reader.result);
            });
        }
    };

    var onError = function (reader:any, deferred:ng.IDeferred<any>, scope:ng.IScope):any {
        return () => {
            scope.$apply(()=> {
                deferred.reject(reader.result);
            });
        }
    };

    var onProgress = function (reader:any, scope:ng.IScope) {
        return (event) => {
            scope.$broadcast("fileProgress", {
                total: event.total,
                loaded: event.loaded
            });
        }
    };

    var getReader = function (deferred:ng.IDeferred<any>, scope:ng.IScope):any {
        var reader = new FileReader();
        reader.onload = onLoad(reader, deferred, scope);
        reader.onerror = onError(reader, deferred, scope);
        reader.onprogress = onProgress(reader, scope);
        return reader;
    };

    var readDataFromFile = function (file, scope:ng.IScope):ng.IPromise<any> {
        if (!checkFileName(file.name)) {
            return;
        }
        var deferred = $q.defer();

        var reader = getReader(deferred, scope);
        reader.readAsText(file);

        return deferred.promise;
    };

    var checkFileName = function (filename:string, scope:ng.IScope) {
        var extension = filename.split('.').pop();
        if(extension == 'har') {
            return true;
        }

        alertService.addAlert('File must be of type *.har.  Please check the file and try again.');
        return false;
    };
    return readDataFromFile;
}]);