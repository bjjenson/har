///<reference path="../../typings/tsd.d.ts"/>
angular.module('har').service('reportService', [function() {
    var data;
    var service = {
        setData :function(d) {
            data = d;
        },
        getData: function() {
            return data;
        }
    };
    return service;
}]);