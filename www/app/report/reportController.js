///<reference path="../../typings/tsd.d.ts"/>
angular.module('har').controller('reportController', [
    '$scope',
    '$timeout',
    '$state',
    'reportService',
    function ($scope, $timeout, $state, reportService) {
        $scope.report = reportService.getData();
        $scope.referredDetail = {};
        function initReports() {
            if (!$scope.report) {
                initSampleData();
            }
            $scope.durationHeight = $scope.report.requestsByDuration[0].values.length * 25;
        }
        $scope.xFunction = function () {
            return function (d) {
                return d.key;
            };
        };
        $scope.countFunction = function () {
            return function (d) {
                return d.values.length;
            };
        };
        $scope.bytesFunction = function () {
            return function (d) {
                var total = 0;
                d.values.forEach(function (i) {
                    total += i;
                });
                return total;
            };
        };
        $scope.clearReport = function () {
            $state.go('landing');
        };
        $scope.toolTipReferrerFunction = function () {
            return function (key, x, y, e, graph) {
                return '<div class="har-tooltip-inner">' +
                    '<p>' + 'Referred:' + y.point.values.length + '</p></br>' +
                    '<p class="har-tooltip-hint">Select for details</p>' +
                    '</div>';
            };
        };
        $scope.tooltipCountFunction = function (unit) {
            return function (key, x, y, e, graph) {
                return '<div class="har-tooltip-inner">' +
                    '<h5 class="har-tooltip-title">' + key + '</h5><hr>' +
                    '<p>' + x + ' ' + unit + '</p>' +
                    '</div>';
            };
        };
        $scope.tooltipDurationFunction = function () {
            return function (key, x, y, e, graph) {
                return '<div class="har-tooltip-inner">' +
                    '<h5 class="har-tooltip-title">' + x + '</h5><hr>' +
                    '<p>' + y + ' ms</p>' +
                    '</div>';
            };
        };
        $scope.$on('elementClick.directive', function (angularEvent, event) {
            var detail = {};
            if (!event.point.values) {
                detail = {};
            }
            else {
                detail = {
                    url: event.label,
                    values: event.point.values
                };
            }
            $timeout(function () {
                $scope.referredDetail = detail;
            });
        });
        initReports();
        function initSampleData() {
            $scope.report = {};
            $scope.report.pageTitle = 'https://github.com/angularjs-nvd3-directives/angularjs-nvd3-directives/blob/master/examples/pie.donut.chart.html';
            $scope.report.requestsByDuration = [
                {
                    key: "Requests by Duration",
                    values: [
                        ["https://github.com/angularjs-nvd3-directives/angularjs-nvd3-directives", 129.765957771107],
                        ["http://localhost:3000/#/report", 101],
                        ["https://docs.angularjs.org/guide/directive", 32.807804682612],
                        ["http://nervgh.github.io/pages/angular-file-upload/examples/simple/", 196.45946739256],
                        ["http://getbootstrap.com/css/#buttons", 0.19434030906893],
                        ["https://github.com/Microsoft/TypeScriptSamples/blob/master/imageboard/routes/index.ts", 98.079782601442],
                        ["http://stackoverflow.com/questions/26287968/meanjs-413-request-entity-too-large", 13.925743130903],
                        ["1https://github.com/angularjs-nvd3-directives/angularjs-nvd3-directives", 129.765957771107],
                        ["1http://localhost:3000/#/report", 101],
                        ["1https://docs.angularjs.org/guide/directive", 32.807804682612],
                        ["1http://nervgh.github.io/pages/angular-file-upload/examples/simple/", 196.45946739256],
                        ["1http://getbootstrap.com/css/#buttons", 0.19434030906893],
                        ["1https://github.com/Microsoft/TypeScriptSamples/blob/master/imageboard/routes/index.ts", 98.079782601442],
                        ["1http://stackoverflow.com/questions/26287968/meanjs-413-request-entity-too-large", 13.925743130903],
                        ["2https://github.com/angularjs-nvd3-directives/angularjs-nvd3-directives", 129.765957771107],
                        ["2http://localhost:3000/#/report", 101],
                        ["2https://docs.angularjs.org/guide/directive", 32.807804682612],
                        ["2http://nervgh.github.io/pages/angular-file-upload/examples/simple/", 196.45946739256],
                        ["2http://getbootstrap.com/css/#buttons", 0.19434030906893],
                        ["2https://github.com/Microsoft/TypeScriptSamples/blob/master/imageboard/routes/index.ts", 98.079782601442],
                        ["3http://stackoverflow.com/questions/26287968/meanjs-413-request-entity-too-large", 13.925743130903],
                        ["3https://github.com/angularjs-nvd3-directives/angularjs-nvd3-directives", 129.765957771107],
                        ["3http://localhost:3000/#/report", 101],
                        ["4https://docs.angularjs.org/guide/directive", 32.807804682612],
                        ["4http://nervgh.github.io/pages/angular-file-upload/examples/simple/", 196.45946739256],
                        ["4http://getbootstrap.com/css/#buttons", 0.19434030906893],
                        ["4https://github.com/Microsoft/TypeScriptSamples/blob/master/imageboard/routes/index.ts", 98.079782601442],
                        ["4http://stackoverflow.com/questions/26287968/meanjs-413-request-entity-too-large", 13.925743130903],
                    ]
                }
            ];
            $scope.report.requestsByType = [
                {
                    key: "text/css",
                    values: [5, 3]
                },
                {
                    key: "application/json",
                    values: [5, 3, 2, 1]
                },
                {
                    key: "text/html",
                    values: [75, 3, 1, 3, 5, 6]
                },
                {
                    key: "application/xml",
                    values: [35, 4]
                },
                {
                    key: "text/zip",
                    values: [5]
                },
                {
                    key: "text/other",
                    values: [59, 3, 6, 8, 9, 10, 3]
                },
                {
                    key: "application/other",
                    values: [5, 3, 3, 3]
                },
                {
                    key: "1text/css",
                    values: [5, 3]
                },
                {
                    key: "1application/json",
                    values: [5, 3, 2, 1]
                },
                {
                    key: "1text/html",
                    values: [75, 3, 1, 3, 5, 6]
                },
                {
                    key: "1application/xml",
                    values: [35, 4]
                },
                {
                    key: "1text/zip",
                    values: [5]
                },
                {
                    key: "1text/other",
                    values: [59, 3, 6, 8, 9, 10, 3]
                },
                {
                    key: "1application/other",
                    values: [5, 3, 3, 3]
                }
            ];
            $scope.report.requestsReferred = [
                {
                    key: "http://getbootstrap.com/assets/flash/ZeroClipboard.swf?noCache=1443826061028",
                    values: ['http://www.google-analytics.com/r/collect?v=1&_v=j39&a=1599042418&t=pageview&_s=1&dl=http%3A%2F%2Fgetbootstrap.com%2Fcss%2F&dr=https%3A%2F%2Fwww.google.com%2F&ul=en-us&de=UTF-8&dt=CSS%20%C2%B7%20Bootstrap&sd=24-bit&sr=2560x1440&vp=648x1043&je=1&fl=19.0%20r0&_u=AACAAAABI~&jid=547378036&cid=1247397921.1443755557&tid=UA-146052-10&_r=1&z=1629642921',
                        'http://www.google-analytics.com/r/collect?v=1&_v=j39&a=1599042418&t=pageview&_s=1&dl=http%3A%2F%2Fgetbootstrap.com%2Fcss%2F&dr=https%3A%2F%2Fwww.google.com%2F&ul=en-us&de=UTF-8&dt=CSS%20%C2%B7%20Bootstrap&sd=24-bit&sr=2560x1440&vp=648x1043&je=1&fl=19.0%20r0&_u=AACAAAABI~&jid=547378036&cid=1247397921.1443755557&tid=UA-146052-10&_r=1&z=1629642921',
                        '333333333333333333333333333333333',
                        'fpoaifjfjiajfij ;aijf;lija ;lijf ;alsijf3i;alijf ja;slije ffjai;']
                },
                {
                    key: "qwerpoiufpoasiuvpoivjaspoidufpoiqpoiefvq",
                    values: ['sdfsssssf6666666666666666', '333333333333333333333333333333333']
                },
                {
                    key: "gbkhqpoinhns;ldinhgdf64g6sd84f6g416d84f63gh514sd",
                    values: ['rrrrrrrr66666666666666666', '333333333333333333333333333333333']
                },
                {
                    key: " 4qpoinqpoifnpqowingpoinapoingpofkmgfpeokmdfpokmgpaokdm",
                    values: ['123456666666666666666666', '333333333333333333333333333333333']
                },
                {
                    key: "vna;slinf;alsinf;laksnd;lfnas;lkdnf;aslknfd;lsakdnf",
                    values: ['98717646666666666666666666', '333333333333333333333333333333333']
                },
            ];
        }
    }]);
//# sourceMappingURL=reportController.js.map