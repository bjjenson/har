///<reference path="../typings/tsd.d.ts"/>

angular.module('har', [
    'ui.router',
    'ui.bootstrap',
    'nvd3ChartDirectives'
]).config(($stateProvider:angular.ui.IStateProvider, $urlRouterProvider:angular.ui.IUrlRouterProvider) => {
    $stateProvider
        .state('landing', {
            url: '/',
            templateUrl: '/app/fileChooser/fileChooserTemplate.html',
            controller: 'fileChooserController'
        }).state('report', {
            url: '/report',
            templateUrl: '/app/report/report.html',
            controller: 'reportController'
        });

    $urlRouterProvider.otherwise('/');
});