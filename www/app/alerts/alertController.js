///<reference path="../../typings/tsd.d.ts"/>
angular.module('har').controller('alertController', ['$scope', 'alertService', function ($scope, alertService) {
        $scope.alerts = alertService.alerts;
        $scope.closeAlert = function (index) {
            alertService.removeAlert(index);
        };
    }]);
//# sourceMappingURL=alertController.js.map