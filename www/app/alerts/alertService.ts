///<reference path="../../typings/tsd.d.ts"/>

angular.module('har').service('alertService', [function () {
    var alerts = [];
    var service = {
        alerts: alerts,
        addAlert: function (msg:string) {
            alerts.push({type: 'danger', msg: msg});
        },
        removeAlert: function(index:number) {
            alerts.splice(index, 1);
        }
    };

    return service;
}]);