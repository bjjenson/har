///<reference path="../../typings/tsd.d.ts"/>
angular.module('har').service('alertService', [function () {
        var alerts = [];
        var service = {
            alerts: alerts,
            addAlert: function (msg) {
                alerts.push({ type: 'danger', msg: msg });
            },
            removeAlert: function (index) {
                alerts.splice(index, 1);
            }
        };
        return service;
    }]);
//# sourceMappingURL=alertService.js.map