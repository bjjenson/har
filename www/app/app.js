///<reference path="../typings/tsd.d.ts"/>
angular.module('har', [
    'ui.router',
    'ui.bootstrap',
    'nvd3ChartDirectives'
]).config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('landing', {
        url: '/',
        templateUrl: '/app/fileChooser/fileChooserTemplate.html',
        controller: 'fileChooserController'
    }).state('report', {
        url: '/report',
        templateUrl: '/app/report/report.html',
        controller: 'reportController'
    });
    $urlRouterProvider.otherwise('/');
});
//# sourceMappingURL=app.js.map