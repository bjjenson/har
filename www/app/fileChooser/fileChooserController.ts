///<reference path="../../typings/tsd.d.ts"/>

interface IFileScope extends ng.IScope {
    readFile():void;
    file:any;
}

angular.module('har').controller('fileChooserController', [
    '$scope',
    'fileReader',
    '$http',
    'reportService',
    'alertService',
    '$state',
    function ($scope:IFileScope, fileReader, $http:ng.IHttpService, reportService, alertService, $state:angular.ui.IStateService) {

    $scope.readFile = function () {
        fileReader($scope.file, $scope)
            .then((result) => {
                $http.post('/har', result).then((response) => {
                    reportService.setData(response.data);
                    console.log(reportService.getData());
                    $state.go('report');
                }, (error) => {
                    alertService.addAlert('There was an error processing the har file.  Please check the content and try again');
                });
            });
    };

}]);