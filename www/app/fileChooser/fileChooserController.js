///<reference path="../../typings/tsd.d.ts"/>
angular.module('har').controller('fileChooserController', [
    '$scope',
    'fileReader',
    '$http',
    'reportService',
    'alertService',
    '$state',
    function ($scope, fileReader, $http, reportService, alertService, $state) {
        $scope.readFile = function () {
            fileReader($scope.file, $scope)
                .then(function (result) {
                $http.post('/har', result).then(function (response) {
                    reportService.setData(response.data);
                    console.log(reportService.getData());
                    $state.go('report');
                }, function (error) {
                    alertService.addAlert('There was an error processing the har file.  Please check the content and try again');
                });
            });
        };
    }]);
//# sourceMappingURL=fileChooserController.js.map