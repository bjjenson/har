///<reference path="../../typings/tsd.d.ts" />

import harParser = require('../../app/harParser');
var fs = require('fs');

describe('HarParser', () => {
    var fileData = fs.readFileSync('./spec/data/jasmine.har', 'utf8');
    var json = JSON.parse(fileData);
    var parser:harParser;
    var pageTitle = 'http://jasmine.github.io/2.0/node.html';
    var duration = 162.9079999984242;

    beforeEach(()=> {
        parser = new harParser(json);
    });

    describe('base statistics', () => {
        it('getEntryCount should return correct amount', ()=> {
            expect(parser.getEntryCount()).toEqual(11);
        });

        it('loadDuration is correct', ()=> {
            expect(parser.getPageLoadDuration()).toEqual(duration);
        });

        it('getTitle is correct', ()=> {
            expect(parser.getTitle()).toEqual(pageTitle);
        });

        it('sorts entries by Duration', ()=> {
            expect(parser.getEntriesByDuration()[0].key).toEqual('Requests by Duration');
            expect(parser.getEntriesByDuration()[0].values[0][0]).toEqual('http://jasmine.github.io/images/jasmine_32x32.ico');
            expect(parser.getEntriesByDuration()[0].values[0][1]).toEqual(120.63099996885285);
        });

        it('groups entries by type', ()=> {
            var stats = parser.getEntriesByType();
            expect(stats[1].key).toEqual('text/css');
            expect(stats[1].values[0]).toEqual(2483);
            expect(stats[1].values[2]).toEqual(5335);
        });

        it('gets requests referred', ()=> {
            var stats = parser.getRequestsReferred();
            expect(stats[0].key).toEqual('http://jasmine.github.io/2.0/node.html');
            expect(stats[0].values.length).toEqual(6);
            expect(stats[0].values[0]).toEqual('http://jasmine.github.io/images/jasmine_32x32.ico');
            expect(stats[0].values[1]).toEqual('http://fonts.googleapis.com/css?family=Lora|Istok+Web:700');
        });

        it('creates full report', ()=> {
            var report = parser.createReport();

            expect(report.pageTitle).toEqual(pageTitle);
            expect(report.pageLoadDuration).toEqual(duration);
            expect(report.requestCount).toEqual(11);
            expect(report.requestsByDuration[0].values.length).toEqual(11);
            expect(report.requestsByType.length).toEqual(5);
            expect(report.requestsReferred.length).toEqual(4);
        });
    });
});